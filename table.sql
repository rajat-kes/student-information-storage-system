create Table info
(
ID varchar2(10) PRIMARY KEY,
Fname varchar2(25) NOT NULL,
Lname varchar2(20) NOT NULL,
Standard varchar2(10) NOT NULL,
DOB varchar2(10) NOT NULL,
MobileNo varchar2(10) NOT NULL,
Email varchar2(10) NOT NULL,
State varchar2(10) NOT NULL,
Address varchar2(10) NOT NULL,
City varchar2(10) NOT NULL
);


create Table reg   
(
username varchar2(10) PRIMARY KEY,
password varchar2(25) NOT NULL,
email varchar2(20) NOT NULL,
mobileno varchar2(10) NOT NULL
);